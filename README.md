﻿# Karkkainen-Sanders

Karkkainen-Sanders is Kärkkäinen,Sanders and Burkhardt algorithm also knows as DC3 published in "Linear Work Suffix Array Construction".

```
Benchmarks:
Intel(R) Core(TM) i3-4160 CPU @
3.60 Ghz
16 GiB
MinGW Release

alice29.txt
Standard text from book 'Alice in Wonderland'
read 100000 bytes
naive 100 times:
naive time 0.0209492 s per loop = 209.492 ns per symbol
algorithm 100 times:
algorithm time 0.00961977 s per loop = 96.1976 ns per symbol
2.17772 x faster
both methods created the same SA

mtpacga
Sample gene, almost random 4/256 values
read 100000 bytes
naive 100 times:
naive time 0.0259791 s per loop = 259.791 ns per symbol
algorithm 100 times:
algorithm time 0.00918968 s per loop = 91.8968 ns per symbol
2.82699 x faster
both methods created the same SA

random.txt
Generated pseudo random text, 64/256 values
read 100000 bytes
naive 100 times:
naive time 0.0173694 s per loop = 173.694 ns per symbol
algorithm 100 times:
algorithm time 0.00674986 s per loop = 67.4986 ns per symbol
2.5733 x faster
both methods created the same SA

abac
Long cycle, problem for naive
read 30000 bytes
naive 1 times:
naive time 0.264991 s per loop = 8833.04 ns per symbol
algorithm 1000 times:
algorithm time 0.00137596 s per loop = 45.8652 ns per symbol
192.587 x faster
both methods created the same SA
```

**License**: Apache 2.0 concerns my files; papers, python or samples have own licenses.