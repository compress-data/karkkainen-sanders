//
// Created by Andrzej Borucki on 2019-07-05
//
#include <vector>
#include <algorithm>
#include <cstdint>
#include <cassert>
#include <tuple>
#include <chrono>
#include <iostream>
#ifdef _MSC_VER
#include <Windows.h>
#else
#include <dirent.h>
#endif

using namespace std;

class stopwatch
{
public:
	chrono::time_point<chrono::high_resolution_clock> a, b;
	void start() { a = chrono::high_resolution_clock::now(); }
	void stop() { b = chrono::high_resolution_clock::now(); }
	double duration()
	{
		chrono::duration<double> elapsed_seconds = b - a;
		return elapsed_seconds.count();
	}
};

typedef uint32_t DictType;
vector<DictType> sa_base;
enum MapType { S_TYPE, L_TYPE };
const DictType Eos = DictType(-1);

//difference from memcmp: is strings are different lengths, and not difference found:
//smaller is shorter string
int bcomp(unsigned char* buf0, unsigned char* buf1, int size0, int size1)
{
    if (size0==0 || size1==0)
    {
        if (size0==0 && size1==0)
            return 0;
        else if (size0==0)
            return -1;
        else return 1;
    }
    else if (buf0[0] != buf1[0])
    {
        if (buf0[0] < buf1[0])
            return -1;
        else
            return 1;
    }
    int memres = memcmp(buf0+1, buf1+1, min(size0, size1)-1);
    if (memres==0)
    {
        if (size0 < size1)
            return -1;
        else if (size0 > size1)
            return 1;
        else return 0;
    }
    else
        return memres;
}

std::string current_working_directory()
{
#ifdef _MSC_VER
	TCHAR NPath[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, NPath);
	wstring wstr = NPath;
	string working_directory(wstr.begin(), wstr.end());
#else
	char* cwd = _getcwd(0, 0);
	std::string working_directory(cwd);
	std::free(cwd);
#endif
	return working_directory;
}

string relativeFolder =
#ifdef _MSC_VER
        "../res/";
#else
        "../../res/";
#endif

struct Source {
	unsigned char* buf;
	int buffer_len;
	int win_start = 0;
	int win_end;

	Source(string filename, int buffer_len) {
		buf = new unsigned char[buffer_len];

		string path = relativeFolder + filename;
		FILE* fp;
		fopen_s(&fp, path.c_str(), "rb");
		if (!fp)
		{
			cout << "can't oppen " << path << endl;
			exit(1);
		}
		fread(buf, 1, buffer_len, fp);
		fclose(fp);
		win_end = buffer_len;
	}
	~Source() {
		delete buf;
	}
};

vector<DictType> initFromSource(Source& source) //@todo template
{
	int string_len = source.win_end - source.win_start;
	vector<DictType> string;
	string.resize(string_len+3);
	for (int i = 0; i < string_len; i++)
		string[i] = source.buf[i + source.win_start];
    string[string_len] = 0;
    string[string_len+1] = 0;
    string[string_len + 2] = 0;
	return string;
}

struct CompObject {
	Source& source;
	unsigned char* begin;
	DictType len;
	CompObject(Source& source) :source(source) {
		begin = source.buf + source.win_start;
		len = source.win_end - source.win_start;
	}
	bool operator()(DictType a, DictType b) {
		return bcomp(begin + a, begin + b, len - a, len - b) < 0;
	}
};

//A naive, slow suffix - array construction algorithm.
//by convention here don't add first element points to end of stream
vector<DictType> naivelyMakeSuffixArray(Source& source)
{
	vector<DictType> sa(source.win_end - source.win_start);
	unsigned char* p = source.buf;
	for (int i = 0; i < source.win_end - source.win_start; i++)
	{
		sa[i] = i;
		p++;
	}
	CompObject co(source);
	sort(sa.begin(), sa.end(), co);
	return sa;
}

struct Sample {
    string filename;
    string desc;
    int numBytes;
    int numLoopsnNaive;
    int numLoopsnAlg;
};

const int numSamples = 4;
const Sample samples[numSamples] = {
        {"alice29.txt", "Standard text from book 'Alice in Wonderland'",100000, 100, 100},
        {"mtpacga","Sample gene, almost random 4/256 values", 100000, 100, 100},
        {"random.txt", "Generated pseudo random text, 64/256 values",100000, 100, 100},
        {"abac", "Long cycle, problem for naive", 30000, 1, 1000}
};


inline bool leq(int a1, int a2, int b1, int b2) { // lexic. order for pairs
	return(a1 < b1 || a1 == b1 && a2 <= b2);
}                                                   // and triples
inline bool leq(int a1, int a2, int a3, int b1, int b2, int b3) {
	return(a1 < b1 || a1 == b1 && leq(a2, a3, b2, b3));
}
// stably sort a[0..n-1] to b[0..n-1] with keys in 0..K from r
static void radixPass(DictType* a, DictType* b, DictType* r, int n, int K)
{ // count occurrences
	DictType* c = new DictType[K + 1];                          // counter array
	for (int i = 0; i <= K; i++) c[i] = 0;         // reset counters
	for (int i = 0; i < n; i++)  c[r[a[i]]]++;    // count occurences
	for (int i = 0, sum = 0; i <= K; i++) { // exclusive prefix sums
		int t = c[i];  c[i] = sum;  sum += t;
	}
	for (int i = 0; i < n; i++) b[c[r[a[i]]]++] = a[i];      // sort
	delete[] c;
}

int GetI(DictType* SA12, int t, int n0) {
	return SA12[t] < n0 ? SA12[t] * 3 + 1 : (SA12[t] - n0) * 3 + 2;
}

// find the suffix array SA of s[0..n-1] in {1..K}^n
// require s[n]=s[n+1]=s[n+2]=0, n>=2
void suffixArray(DictType* s, DictType* SApar, int n, int K) {
	int n0 = (n + 2) / 3, n1 = (n + 1) / 3, n2 = n / 3, n02 = n0 + n2;
	DictType* R = new DictType[n02 + 3];  R[n02] = R[n02 + 1] = R[n02 + 2] = 0;
	DictType* SA12 = new DictType[n02 + 3]; SA12[n02] = SA12[n02 + 1] = SA12[n02 + 2] = 0;
	DictType* s0 = new DictType[n0];
	DictType* SA0 = new DictType[n0];

	// generate positions of mod 1 and mod  2 suffixes
	// the "+(n0-n1)" adds a dummy mod 1 suffix if n%3 == 1
	for (int i = 0, j = 0; i < n + (n0 - n1); i++)
		if (i % 3 != 0)
			R[j++] = i;

	// lsb radix sort the mod 1 and mod 2 triples
	radixPass(R, SA12, s + 2, n02, K);
	radixPass(SA12, R, s + 1, n02, K);
	radixPass(R, SA12, s, n02, K);

	// find lexicographic names of triples
	int name = 0, c0 = -1, c1 = -1, c2 = -1;
	for (int i = 0; i < n02; i++) {
		if (s[SA12[i]] != c0 || s[SA12[i] + 1] != c1 || s[SA12[i] + 2] != c2) {
			name++;  c0 = s[SA12[i]];  c1 = s[SA12[i] + 1];  c2 = s[SA12[i] + 2];
		}
		if (SA12[i] % 3 == 1) { R[SA12[i] / 3] = name; } // left half
		else { R[SA12[i] / 3 + n0] = name; } // right half
	}

	// recurse if names are not yet unique
	if (name < n02) {
		suffixArray(R, SA12, n02, name);
		// store unique names in s12 using the suffix array 
		for (int i = 0; i < n02; i++) R[SA12[i]] = i + 1;
	}
	else // generate the suffix array of s12 directly
		for (int i = 0; i < n02; i++) SA12[R[i] - 1] = i;

	// stably sort the mod 0 suffixes from SA12 by their first character
	for (int i = 0, j = 0; i < n02; i++) if (SA12[i] < n0) s0[j++] = 3 * SA12[i];
	radixPass(s0, SA0, s, n0, K);

	// merge sorted SA0 suffixes and sorted SA12 suffixes
	for (int p = 0, t = n0 - n1, k = 0; k < n; k++) {
		int i = GetI(SA12, t, n0); // pos of current offset 12 suffix
		int j = SA0[p]; // pos of current offset 0  suffix
		if (SA12[t] < n0 ?
			leq(s[i], R[SA12[t] + n0], s[j], R[j / 3]) :
			leq(s[i], s[i + 1], R[SA12[t] - n0 + 1], s[j], s[j + 1], R[j / 3 + n0]))
		{ // suffix from SA12 is smaller
			SApar[k] = i;  t++;
			if (t == n02) { // done --- only SA0 suffixes left
				for (k++; p < n0; p++, k++) SApar[k] = SA0[p];
			}
		}
		else {
			SApar[k] = j;  p++;
			if (p == n0) { // done --- only SA12 suffixes left
				for (k++; t < n02; t++, k++) SApar[k] = GetI(SA12, t, n0);
			}
		}
	}
	delete[] R; delete[] SA12; delete[] SA0; delete[] s0;
}

int main()
{
    cout << "current directory=" << current_working_directory() << endl;
	cout << "search res in " << relativeFolder << endl;
    for (int i=0; i<numSamples; i++)
    {
        stopwatch st;
        cout << samples[i].filename << endl;
        cout << samples[i].desc << endl;
        cout << "read " << samples[i].numBytes << " bytes " << endl;

        Source source(samples[i].filename, samples[i].numBytes);

        cout << "naive " << samples[i].numLoopsnNaive << " times:" << endl;
        st.start();
        for (int j=0; j<samples[i].numLoopsnNaive; j++) {
            sa_base = naivelyMakeSuffixArray(source);
        }
        st.stop();
        double naiveTime = st.duration()/samples[i].numLoopsnNaive;
        double perSymbol = naiveTime/ samples[i].numBytes *1e9;
        cout << "naive time " <<  naiveTime << " s per loop" << " = " << perSymbol << " ns per symbol" << endl;

        cout << "algorithm " << samples[i].numLoopsnAlg << " times:" << endl;
		DictType* sa = new DictType[samples[i].numBytes];
        st.start();
        for (int j=0; j<samples[i].numLoopsnAlg; j++) {
            vector<DictType> string = initFromSource(source);			
            suffixArray(string.data(), sa, string.size()-3, 256);
        }
        st.stop();
        double algTime = st.duration()/samples[i].numLoopsnAlg;
        perSymbol = algTime/ samples[i].numBytes *1e9;
        cout << "algorithm time " <<  algTime << " s per loop" << " = " << perSymbol << " ns per symbol" << endl;
        cout << naiveTime/algTime << " x faster " << endl;

        if (bcomp((unsigned char*)sa_base.data(), (unsigned char*)sa, sa_base.size()* sizeof(DictType), samples[i].numBytes* sizeof(DictType))==0)
            cout << "both methods created the same SA" <<endl;
        else
            cout << "Error, methods created differ SA" <<endl;
		delete sa;
        cout << endl;
    }
}
